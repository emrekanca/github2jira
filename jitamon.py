#!/usr/bin/python
"""
add webhook
curl -X POST -u vedit -H "Content-Type: application/json" -d '{"name":"web","config":{"url":"https://6e6c794c23c2649fc6fbf53f0ab31673:17e7d6a14bc0c67ae2a56a8f01863619@50.17.88.204:8080/87df2cd1570fd297de238aeee667fe0a", "content_type":"json"},"events":["commit_comment","issue_comment","pull_request_review_comment"],"active":true}' https://api.github.com/repos/vedit/apitest/hooks

get webhook
curl -u vedit https://api.github.com/repos/vedit/apitest/hooks

"""
__author__ = 'hexenoid'
import json
import re
import requests
import logging
import ConfigParser
from daemon import runner
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from flask import Flask, request, Response, jsonify
from functools import wraps
from abc import ABCMeta, abstractmethod


# set logging stuff right
logger = logging.getLogger("DaemonLog")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler('g2j.log')
handler.setFormatter(formatter)
logger.addHandler(handler)

# parse configuration
config = ConfigParser.ConfigParser()
app_c = open('app.conf', 'rb')
app_c.readline()
config.readfp(app_c)
githubUsername = config.get('GitHubWebHookAuth', 'username')
githubPassword = config.get('GitHubWebHookAuth', 'password')
jiraUser = config.get('JiraConn', 'username')
jiraPassword = config.get('JiraConn',  'password')
jiraUrl = config.get('JiraConn',  'url')
host = config.get('App', 'host')
port = config.get('App', 'port')
inProgressId = config.get('Transitions', 'pre-code-review')
inCodeReviewId = config.get('Transitions', 'in-code-review')
doneId = config.get('Transitions', 'after-code-review')
app_c.close()


# add post request handler
receiver = Flask(__name__)


@receiver.route('/test', methods=['GET'])
def endpoint_healthcheck():

    print "OK"
    return "OK"


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    6e6c794c23c2649fc6fbf53f0ab31673:17e7d6a14bc0c67ae2a56a8f01863619@<url-here>
    """
    logger.info(username + " " + password + " tried connection")
    return username == githubUsername and password == githubPassword


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            auth = request.authorization
            if not auth or not check_auth(auth.username, auth.password):
                logger.info("auth failed: " + auth.username)
                return authenticate()
            return f(*args, **kwargs)
        except AttributeError:
            return authenticate()
    return decorated


class RepoToJira:
    __metaclass__ = ABCMeta
    @abstractmethod
    def receive_hook(self):
        raise NotImplementedError

class GithubToJira(RepoToJira):
    def receive_hook(self):
        logger.info('Received github connection')
    #     json_data=open('/home/emre/Desktop/pull.json')
        json_data = request.json
        data = json_data
        transitionId = 0
        if ( (data['action'] == "opened") or (data['action'] == "reopened")): #Issue should be moved to In-Code-review
            transitionId = inCodeReviewId
        else: #closed
            if data['pull_request']['merged'] == False: # Just closed. No merge. Issue should be moved In-progress
                transitionId = inProgressId
            else: #Merged! So closed. Issue should be moved to Done
                transitionId = doneId
        body = data['pull_request']['body']
        title = data['pull_request']['title']
        issue = ""
        r = re.search(r'closes\s+(\S+)', body)
        if r:
            logger.debug('body matched')
            issue = r.group(1)
        else:
            r2 = re.search(r'closes\s+(\S+)', title)
            if r2:
                issue = r2.group(1)
            else:
                logger.error("Error - No issue found")
        post_jira_transition(issue, transitionId)
        return "OK"


@receiver.route('/receive', methods=['POST'])
@requires_auth
def receive():
    gtj = GithubToJira()
    return gtj.receive_hook()

def post_jira_transition(issue, transitionId):
    url = "http://" + jiraUrl + "/rest/api/2/issue/" + issue + "/transitions"
    jiraData = '{"transition": {"id": "' + transitionId + '"}}'
    headers = {'Accept': 'application/json', "Content-type": "application/json"}
    result = requests.post(url, data = jiraData, headers = headers, auth=(jiraUser, jiraPassword))
    if 300 > result.status_code >= 200:
        logger.info('Jira ticket succesfully updated \n' + url)
    logger.error('http status code: ' + str(result.status_code) + '\ncheck credentials\ncan\'t post that comment.')


def test():
    try:
        url = "http://" + jiraUrl + "/rest/api/2/mypermissions"
        headers = {'Accept': 'application/json', "Content-type": "application/json"}
        result = requests.get(url, headers=headers, auth=(jiraUser, jiraPassword))
        print(result.status_code)
        if 300 > result.status_code >= 200:
            print "Jira connection tested - OK"
            havePermission = json.loads(result.content)["permissions"]["MOVE_ISSUE"]["havePermission"]
            if not havePermission:
                print "User doesn't have permission to move the tickets. Exitting"
                exit()
            else:
                print "User permission tested - OK"
        else:
            print "Could not connect. Exitting"
            print(result.status_code)
            exit()
        # url = "http://" + jiraUrl + "/rest/api/2/search"
        # headers = {'Accept': 'application/json', "Content-type": "application/json"}
        # result = requests.get(url, headers = headers, auth=(jiraUser, jiraPassword))
        # if 300 > result.status_code >= 200:
        #     print "Jira connection OK"
        #     randomIssue = json.loads(result.content)["issues"][0]["key"]
        #     issueData = requests.get("http://46.51.206.105:8080/rest/api/2/issue/LEAF-2", headers=headers, auth=(jiraUser, jiraPassword))
        #     print (issueData.content)
        #     print str(issueData.status_code)
        #     print str(result.status_code)

    except:
        print "Could not connect to jira. Exitting"
        exit()

def prod():
    class G2JDaemon():
        def __init__(self):
            self.stdin_path = '/dev/null'
            self.stdout_path = '/dev/tty'
            self.stderr_path = '/dev/tty'
            self.pidfile_path = '/tmp/gitjira.pid'
            self.pidfile_timeout = 5

        def run(self):
            server = HTTPServer(WSGIContainer(receiver))
            logger.info("Starting daemon at " + str(server))
            server.listen(port)
            test()
            IOLoop.instance().start()


    g2j = G2JDaemon()
    logger.debug("Initialized g2j daemon at " + str(g2j))
    daemon_runner = runner.DaemonRunner(g2j)
    logger.info("Starting g2j listening port " + port)
    daemon_runner.daemon_context.files_preserve = [handler.stream]


    # lockfile = open("/tmp/myapp.pid")
    # if lockfile.read() != 1:
    #     test()
    # else:
    #     runner.make_pidlockfile('/tmp/myapp.pid', 1)

    # if not lockfile.is_locked():
    #     test()
    # if not runner.lockfile.FileLock.is_locked():


    try:
        daemon_runner.do_action()
    except runner.DaemonRunnerStopFailureError:
        print "Jitamon is NOT running"
        logger.warning("Jitamon is NOT running")
    except runner.lockfile.LockTimeout:
        print "Jitamon IS running"
        logger.warning("Jitamon IS running")

def dev():
    receiver.run(host='0.0.0.0', debug=True)


if __name__ == '__main__':
    prod()

