__author__ = 'hexenoid'
import ConfigParser

config = ConfigParser.RawConfigParser()

# When adding sections or items, add them in the reverse order of
# how you want them to be displayed in the actual file.
# In addition, please note that using RawConfigParser's and the raw
# mode of ConfigParser's respective set functions, you can assign
# non-string values to keys internally, but will receive an error
# when attempting to write to a file or when you get it in non-raw
# mode. SafeConfigParser does not allow such assignments to take place.
config.add_section('GithubConn')
config.set('GithubConn', 'token', '6dbe562cadff77aece448da8b0fb934622c68ac6')
config.set('GithubConn', 'user', 'Bookish')
config.add_section('Repos')
config.set('Repos', 'repo1', 'bookish.com')
config.set('Repos', 'repo2', 'bookish')
config.set('Repos', 'repo3', 'CMS')
config.set('Repos', 'repo4', 'bookish.web')
config.add_section('App')
config.set('App', 'loglevel', 0)


# Writing our configuration file to 'example.cfg'
with open('example.cfg', 'wb') as configfile:
    config.write(configfile)